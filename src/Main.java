/**
 * Main класс, выполняет вычисление выражения, содержащего дроби.
 * Результат программы записывается в текстовый файл
 * Демонстрирует работу классов:
 * - Expression;
 * - ExpressionProcedures;
 * - Fraction;
 * - FractionProcedures;
 *
 * @author Isheykin A.A.
 */
public class Main {
        public static void main(String[] args) throws Exception {
            String expression_string = ExpressionProcedures.input();
            ExpressionProcedures.scanExpression(expression_string);
    }
}
