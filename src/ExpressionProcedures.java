import java.util.Scanner;
import java.util.regex.Matcher;

/**
 * Класс ExpressionProcedures, предназначеный для работы с объектом типа "Expression"
 * Содержит методы:
 * - input()
 * - scanExpression(String expression_string)
 * - signScan(String expression)
 * - searchPositionSign(String expression)
 * - calculateExpression(Expression expression)
 * - scanFirstNumber(String expression_string, int signNum)
 * - scanSecondNumber(String expression_string, int signNum)
 */

    public class ExpressionProcedures {
    private static Scanner scanner = new Scanner(System.in);
    // public static int[] numberSequence = {0,1,2,3,4,5,6,7,8,9};

    public static String input() throws Exception {
        System.out.println("Введите выражение\nСправка:\nДроби в выражении пишутся по типу:\n (числитель)/(знаменатель)");
        String line = scanner.nextLine();
        if (line.matches(("^[a-zA-Zа-яА-Я]"))) {
            throw new Exception("Exception!");
        }

        // todo: реализовать исключение для некорректно введеной строки
//        try {
//            char sign = ExpressionProcedures.signScan(line);
//            int positionSign = ExpressionProcedures.searchPositionSign(line);
//            Expression expression = new Expression(sign, FractionProcedures.convert(ExpressionProcedures.scanFirstNumber(line, positionSign)),
//                    FractionProcedures.convert(ExpressionProcedures.scanSecondNumber(line, positionSign)));
//        } catch (ExceptionInInitializerError e) {
//            line = input();
//        }
        return line;
    }

    public static void scanExpression(String expression_string) {
        Expression expression = new Expression();
        expression.setExpressionSign(ExpressionProcedures.signScan(expression_string));
        int positionSign = ExpressionProcedures.searchPositionSign(expression_string);
        String firstFraction = (ExpressionProcedures.scanFirstNumber(expression_string, positionSign));
        expression.setFirstFraction(FractionProcedures.convert(firstFraction));
        String secondFraction = (ExpressionProcedures.scanSecondNumber(expression_string, positionSign));
        expression.setSecondFraction(FractionProcedures.convert(secondFraction));
        ExpressionProcedures.calculateExpression(expression);
    }

    public static char signScan(String expression) {
        // Добавить проверку на найденый знак. если не нашел - выбрасывать исключение
        String signs = "*:+-";
        char searchableSign;
        for (int operandNumber = 0; operandNumber < signs.length(); operandNumber++) {
            for (int signNumber = 0; signNumber < expression.length(); signNumber++) {
                searchableSign = signs.charAt(operandNumber);
                if (expression.charAt(signNumber) == searchableSign) {
                    return expression.charAt(signNumber);
                } else {
                    continue;
                }
            }
        }
        return '?';
    }

    public static int searchPositionSign(String expression) {
        String signs = "*:+-";
        char searchableSign;
        for (int operandNumber = 0; operandNumber < signs.length(); operandNumber++) {
            for (int signNumber = 0; signNumber < expression.length(); signNumber++) {
                searchableSign = signs.charAt(operandNumber);
                if (expression.charAt(signNumber) == searchableSign) {
                    return signNumber;
                } else continue;
            }
        }
        return 0;
    }


    public static void calculateExpression(Expression expression) {
        Fraction result = FractionProcedures.calculate(expression.getFirstFraction(),
                expression.getSecondFraction(),
                expression.getExpressionSign());
        System.out.println(result);
    }

    public static String scanFirstNumber(String expression_string, int signNum) {
        String firstNumber = "";
        boolean isExpressionEnd = false;
        try {
            while (isExpressionEnd == false) {
                for (int signNumber = signNum - 1; isExpressionEnd == false; signNumber--) {
//                     if (expression_string.matches("[0-9]")) {
                    if (expression_string.charAt(signNumber) == '1' ||
                            expression_string.charAt(signNumber) == '2' ||
                            expression_string.charAt(signNumber) == '3' ||
                            expression_string.charAt(signNumber) == '4' ||
                            expression_string.charAt(signNumber) == '5' ||
                            expression_string.charAt(signNumber) == '6' ||
                            expression_string.charAt(signNumber) == '7' ||
                            expression_string.charAt(signNumber) == '8' ||
                            expression_string.charAt(signNumber) == '9' ||
                            expression_string.charAt(signNumber) == '0' ||
                            expression_string.charAt(signNumber) == ',' ||
                            expression_string.charAt(signNumber) == '/') {
                        firstNumber = expression_string.charAt(signNumber) + firstNumber;
                        if (signNumber == 0) {
                            isExpressionEnd = true;
                            break;
                        }
                    } else {
                        isExpressionEnd = true;
                        break;
                    }
                }
                }
        } catch (java.lang.StringIndexOutOfBoundsException e) {
            throw new StringIndexOutOfBoundsException("Выход за пределы строки!\n" +
                    "Возможные ошибки:\n" +
                    "1.signNumber имеет значение 0 следовательно цикл выходит за пределы строки.Может быть вызвано:\n" +
                    "a) Некорректно введенной строкой");
        }
        return firstNumber;
    }



        public static String scanSecondNumber(String expression_string, int signNum) {
            String secondNumber = "";
            boolean isExpressionEnd = false;
            while (isExpressionEnd == false) {
                for (int signNumber = signNum + 1; isExpressionEnd == false; signNumber++) {
                    if (expression_string.charAt(signNumber) == '1' ||
                            expression_string.charAt(signNumber) == '2' ||
                            expression_string.charAt(signNumber) == '3' ||
                            expression_string.charAt(signNumber) == '4' ||
                            expression_string.charAt(signNumber) == '5' ||
                            expression_string.charAt(signNumber) == '6' ||
                            expression_string.charAt(signNumber) == '7' ||
                            expression_string.charAt(signNumber) == '8' ||
                            expression_string.charAt(signNumber) == '9' ||
                            expression_string.charAt(signNumber) == '0' ||
                            expression_string.charAt(signNumber) == ',' ||
                            expression_string.charAt(signNumber) == '/') {
                        secondNumber = secondNumber + expression_string.charAt(signNumber);
                        if (signNumber == expression_string.length() - 1) {
                            isExpressionEnd = true;
                            break;
                        }
                    } else {
                        isExpressionEnd = true;
                        break;
                    }
                }
            }
            return secondNumber;
        }
    }
